import 'package:flutter/material.dart';
import 'package:smothie_page/ui/shared/buttons/custom_icon_button.dart';

class SmoothieAppBar extends StatelessWidget with PreferredSizeWidget {
  final appBarIconSize = 32.0;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 35.0, right: 6.0, bottom: 10.0, left: 6.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          CustomIconButton(
            size: appBarIconSize,
            buttonFunction: () {},
            icon: Icons.account_circle_outlined,
          ),
          CustomIconButton(
            size: appBarIconSize,
            buttonFunction: () {},
            icon: Icons.shopping_bag_outlined,
          ),
        ],
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(70.0);
}
