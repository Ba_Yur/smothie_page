import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DoubleButton extends StatefulWidget {
  @override
  _DoubleButtonState createState() => _DoubleButtonState();
}

class _DoubleButtonState extends State<DoubleButton> {
  final double borderRadius = 10;
  int indexActive = 1;

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        color: Colors.black.withOpacity(0.1),
        borderRadius: BorderRadius.circular(borderRadius),
      ),
      child: Row(
        children: [
          oneOfButtons(
            label: 'Bottles',
            icon: CupertinoIcons.ant_fill,
            buttonIndex: 1,
            indexActive: indexActive,
            buttonFunction: () {
              setState(() {
                indexActive = 1;
              });
            },
          ),
          oneOfButtons(
            label: 'Boxes',
            icon: CupertinoIcons.cube_box_fill,
            buttonIndex: 2,
            indexActive: indexActive,
            buttonFunction: () {
              setState(() {
                indexActive = 2;
              });
            },
          ),
        ],
      ),
    );
  }
}

final TextStyle buttonTextStyle = TextStyle(fontSize: 12.0, fontWeight: FontWeight.w400);

class oneOfButtons extends StatelessWidget {
  final int buttonIndex;
  final int indexActive;
  final Function buttonFunction;
  final IconData icon;
  final String label;

  const oneOfButtons({
    @required this.buttonIndex,
    @required this.indexActive,
    @required this.buttonFunction,
    @required this.icon,
    @required this.label,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SizedBox(
        height: 35.0,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: Material(
            color: indexActive == buttonIndex ? Colors.black : Colors.transparent,
            child: InkWell(
              onTap: () {
                buttonFunction();
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    icon,
                    size: 15.0,
                    color: indexActive == buttonIndex ? Colors.white : Colors.black,
                  ),
                  SizedBox(
                    width: 5.0,
                  ),
                  Text(
                    label,
                    style: buttonTextStyle.copyWith(
                      color: indexActive == buttonIndex ? Colors.white : Colors.black,
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
