import 'package:flutter/material.dart';

class CustomIconButton extends StatelessWidget {
  final double size;
  final Function buttonFunction;
  final IconData icon;
  final Color color;

  const CustomIconButton({
    @required this.size,
    @required this.buttonFunction,
    @required this.icon,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular((size + 10.0) / 2),
      child: SizedBox(
        height: size + 10.0,
        width: size + 10.0,
        child: Material(
          child: InkWell(
            onTap: () {},
            child: Icon(
              icon,
              size: size,
              color: color,
            ),
          ),
        ),
      ),
    );
  }
}
