import 'package:flutter/material.dart';

class ProductCardWidget extends StatelessWidget {
  final String imageUrl;
  final String name;
  final String description;
  final bool isNew;
  final double price;
  final int volume;
  final Color textColor;
  final int discount;

  const ProductCardWidget({
    @required this.imageUrl,
    @required this.name,
    @required this.description,
    @required this.isNew,
    @required this.price,
    @required this.volume,
    @required this.textColor,
    this.discount,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: SizedBox(
        height: 400.0,
        child: DecoratedBox(
          decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(imageUrl),
              ),
              borderRadius: BorderRadius.circular(15.0),
              color: Colors.red),
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    discount != null
                        ? SizedBox(
                            height: 25.0,
                            width: 100.0,
                            child: DecoratedBox(
                              decoration: BoxDecoration(color: textColor, borderRadius: BorderRadius.circular(8.0)),
                              child: Center(
                                child: Text(
                                  '$discount% OFF',
                                  style: titleStyle.copyWith(color: Colors.white, fontSize: 14),
                                ),
                              ),
                            ),
                          )
                        : SizedBox(),
                    Spacer(),
                    isNew
                        ? SizedBox(
                            height: 25.0,
                            width: 60.0,
                            child: DecoratedBox(
                              decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(8.0)),
                              child: Center(
                                child: Text(
                                  'New',
                                  style: titleStyle.copyWith(color: textColor, fontSize: 14),
                                ),
                              ),
                            ),
                          )
                        : SizedBox(),
                  ],
                ),
                Spacer(),
                Text(
                  name,
                  style: titleStyle.copyWith(color: textColor),
                ),
                Text(description, style: titleStyle.copyWith(color: textColor.withOpacity(0.7), fontSize: 14.0)),
                Row(
                  children: [
                    Text(
                      '\$ ${price.toStringAsFixed(2)}',
                      style: titleStyle,
                    ),
                    Text(' / $volume ml.', style: titleStyle.copyWith(color: Colors.white.withOpacity(0.4), fontSize: 14)),
                    Spacer(),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(20.0),
                      child: SizedBox(
                        height: 40.0,
                        width: 40.0,
                        child: Material(
                          color: Colors.white38,
                          child: InkWell(
                            onTap: () {},
                            child: Icon(
                              Icons.add,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

final TextStyle titleStyle = TextStyle(color: Colors.white, fontSize: 24.0, fontWeight: FontWeight.w700);
