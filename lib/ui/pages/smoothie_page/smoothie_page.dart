import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:smothie_page/ui/pages/smoothie_page/product_card_widget.dart';
import 'package:smothie_page/ui/shared/buttons/double_button.dart';
import 'package:smothie_page/ui/shared/smoothie_appbar.dart';

class SmoothiePage extends StatelessWidget {
  final String title = 'Choose your favorite smoothie';
  final String subTitle = 'Freshness portion in every bottle';

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.white,
    ));
    return Scaffold(
      appBar: SmoothieAppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: titleStyle,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
            const SizedBox(
              height: 10.0,
            ),
            Text(
              subTitle,
              style: subtitleStyle,
              overflow: TextOverflow.ellipsis,
            ),
            const SizedBox(
              height: 10.0,
            ),
            DoubleButton(),
            const SizedBox(
              height: 15,
            ),
            Expanded(
              child: ListView(
                children: [
                  ProductCardWidget(
                    name: 'Morning orchard',
                    description: 'Apple, Broccoli, Dill',
                    imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQcUglP1VjoCb1lLH2QOIiYxMn2qkrReRgVNA&usqp=CAU',
                    isNew: true,
                    price: 9.90,
                    volume: 300,
                    textColor: Colors.green,
                    discount: 20,
                  ),
                  ProductCardWidget(
                    name: 'Morning orchard',
                    description: 'Apple, Broccoli, Dill',
                    imageUrl: 'https://media.istockphoto.com/photos/colorful-smoothies-in-plastic-bottles-picture-id611602808?k=6&m=611602808&s=170667a&w=0&h=10XT_bh5zKgCn2KsgYrEvoihkH9gb2MkmHmbzHfZLbA=',
                    isNew: false,
                    price: 9.90,
                    volume: 300,
                    textColor: Colors.blue,
                    discount: 30,
                  ),
                  ProductCardWidget(
                    name: 'Morning orchard',
                    description: 'Apple, Broccoli, Dill',
                    imageUrl: 'https://cdn.shopify.com/s/files/1/0815/1831/products/the-smoothie-bombs-500ml-bottle-glass-flask-4054100541534_900x.png?v=1569231851',
                    isNew: true,
                    price: 9.90,
                    volume: 300,
                    textColor: Colors.red,
                    discount: 30,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

final TextStyle titleStyle = TextStyle(color: Colors.black, fontSize: 24.0, fontWeight: FontWeight.w700);

final TextStyle subtitleStyle = TextStyle(color: Colors.black.withOpacity(0.6), fontSize: 12.0, fontWeight: FontWeight.w400);
