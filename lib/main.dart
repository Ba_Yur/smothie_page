import 'package:flutter/material.dart';
import 'package:smothie_page/ui/pages/smoothie_page/smoothie_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: SmoothiePage(),
    );
  }
}
